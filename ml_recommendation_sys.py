#!/usr/bin/python3

import sys
import pandas
import argparse
import subprocess

# Import CountVectorizer and create the count matrix
from sklearn.feature_extraction.text import CountVectorizer
# Compute the Cosine Similarity matrix based on the count_matrix
from sklearn.metrics.pairwise import cosine_similarity

PARSER = argparse.ArgumentParser()
PARSER.add_argument('-p', '--package', help='Package name.')
PARSER.add_argument('-a', '--app', help='App name.')
ARGS = PARSER.parse_args()


class RecommendationSystem():

    # Function to convert all strings to lower case and strip names of spaces
    def clean_data(self, x):
        if isinstance(x, list):
            return [str.lower(i.replace(" ", "")) for i in x]
        else:
            # Check if director exists. If not, return empty string
            if isinstance(x, str):
                return str.lower(x.replace(" ", ""))
            else:
                return ''

    def create_soup(self, x):
        return ' '.join(x['category']) + ' ' + ' '.join(x['public_classification'])

    # Function that takes in movie title as input and outputs most similar movies
    def get_recommendations(self, title, cosine_sim, apps, field):
        # Reset index of your main DataFrame and construct reverse mapping as before
        movies = apps.reset_index()
        indices = pandas.Series(apps.index, index=movies[field])

        # Get the index of the movie that matches the title
        idx = indices[title]

        # Get the pairwsie similarity scores of all movies with that movie
        sim_scores = list(enumerate(cosine_sim[idx]))
        # Sort the movies based on the similarity scores
        sim_scores = sorted(sim_scores, key=lambda x: x[1], reverse=True)
    
        # Get the scores of the 10 most similar movies
        sim_scores = sim_scores[1:11]

        # Get the movie indices
        movie_indices = [i[0] for i in sim_scores]

        # Return the top 10 most similar movies
        return movies[['name', 'package']].iloc[movie_indices]

    def run(self):
        if ARGS.app is None and ARGS.package is None:
            subprocess.call(['python3', 'ml_recommendation_sys.py', '-h'])
            sys.exit(0)

        # Load Apps Metadata
        apps = pandas.read_csv('list_apps.csv', low_memory=False)

        # Apply clean_data function to your features.
        features = ['category', 'public_classification']

        # Replace NaN with an empty string
        apps['name'] = apps['name'].fillna('')
        apps['package'] = apps['package'].fillna('')
        apps['category'] = apps['category'].fillna('')
        apps['public_classification'] = apps['public_classification'].fillna('')
        apps['category'] = apps['category'].apply(lambda field: field.split('&'))

        for feature in features:
            apps[feature] = apps[feature].apply(self.clean_data)
        apps['soup'] = apps.apply(self.create_soup, axis=1)
        count = CountVectorizer(stop_words='english')
        count_matrix = count.fit_transform(apps['soup'])
        cosine_sim = cosine_similarity(count_matrix, count_matrix)
        result=pandas.DataFrame(cosine_sim)
        field = 'name'
        item = ARGS.app
        if ARGS.app is None:
            field = 'package'
            item = ARGS.package

        print(item)
        print(self.get_recommendations(item, cosine_sim, apps, field))


if __name__ == "__main__":
    RecommendationSystem().run()
