from sklearn.model_selection import train_test_split
import pandas as pd 
from sklearn import preprocessing
from sklearn import linear_model

data=pd.read_csv('Iris.csv')

features = list(data.columns[1:5])            # SepalLengthCm	SepalWidthCm	PetalLengthCm	PetalWidthCm	
target = data.columns[5]                      # Species


# store feature matrix in "X"
X = data.iloc[:,1:5]                          # slicing: all rows and 1 to 4 cols
# store response vector in "y"
y = data.iloc[:,5]                            # slicing: all rows and 5th col

le = preprocessing.LabelEncoder()
le.fit(y)
y=le.transform(y)
data['EncodedSpecies'] = y


clf = linear_model.LinearRegression()
clf.fit(X, y)
value=clf.predict([[6, 3, 6, 1]])
print( data.loc[data['EncodedSpecies'] == int(value), 'Species'].values[0])
