import pandas as pd 
from sklearn import preprocessing
from sklearn import metrics
from sklearn.naive_bayes import GaussianNB

def converssion(data):
    return {
        1:"Basico",
        2:"Moderado",
        3:"Trendic_Topic",
        4:"Empresarial",
        5:"Friki_Tecnologico"
    }[data]


data=pd.read_csv('data_sample.csv')


# store feature matrix in "X"
X = data.iloc[:,0:11]                          # slicing: all rows and 1 to 4 cols
# store response vector in "y"
y = data.iloc[:,11]                            # slicing: all rows and 5th col

clf = GaussianNB()
clf.fit(X,y)
pred=clf.predict([[0,113295423,0,653437782,0,0,0,216611520,14845961,223046647,0]])
print(converssion(int(pred)))
