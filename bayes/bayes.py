from sklearn.model_selection import train_test_split
import pandas as pd 
from sklearn import preprocessing
from sklearn.naive_bayes import MultinomialNB
from sklearn.metrics import accuracy_score

data=pd.read_csv('Iris.csv')

features = list(data.columns[1:5])            # SepalLengthCm	SepalWidthCm	PetalLengthCm	PetalWidthCm	
target = data.columns[5]                      # Species


# store feature matrix in "X"
X = data.iloc[:,1:5]                          # slicing: all rows and 1 to 4 cols
# store response vector in "y"
y = data.iloc[:,5]                            # slicing: all rows and 5th col

clf = MultinomialNB()
clf.fit(X,y)
pred=clf.predict([[6, 3, 6, 1]])
print(pred)