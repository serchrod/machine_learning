# Machine Learning Algorithms Collections

This projects has a mixed examples of machine learning algorithms.

# Machine Learning Algorithms:
- Lasso
- kmeans
- K nearest neighbor
- linear_model
- Naive Bayes(Gaussian and Multinomial)

### Machine Learning complementary algorithms:

- Cosine Similarity
- Kd Tree algorithms
