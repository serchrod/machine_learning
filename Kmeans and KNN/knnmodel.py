import pandas as pd 
from sklearn import preprocessing
from sklearn.neighbors import KDTree
from sklearn.neighbors import KNeighborsClassifier
from sklearn.metrics import average_precision_score
import matplotlib.pyplot as plt
import datetime

data=pd.read_csv('Iris.csv')

features = list(data.columns[1:5])            # SepalLengthCm	SepalWidthCm	PetalLengthCm	PetalWidthCm	
target = data.columns[5]                      # Species


# store feature matrix in "X"
X = data.iloc[:,1:5]                          # slicing: all rows and 1 to 4 cols
# store response vector in "y"
y = data.iloc[:,4]                            # slicing: all rows and 5th col

le = preprocessing.LabelEncoder()
le.fit(y)
y=le.transform(y)

# new col
data['EncodedSpecies'] = y
knn = KNeighborsClassifier(n_neighbors=100)
start=datetime.time.hour
knn.fit(X, y)
value = knn.predict([[4.3, 3.0, 1.1, 0.1]])
print(value)
print( data.loc[data['EncodedSpecies'] == int(value), 'Species'].values[0])


#agregando otra prediccion
#X_new = [[3, 5, 4, 2], [5, 4, 3, 2]]
##value=knn.predict(X_new)
#print('Predicted Class' , data.loc[data['EncodedSpecies'] == int(value[0]), 'Species'].values[0])
#print('Predicted Class' , data.loc[data['EncodedSpecies'] == int(value[1]), 'Species'].values[0])

#kypred = knn.predict(X)
#print(metrics.accuracy_score(y, kypred))
