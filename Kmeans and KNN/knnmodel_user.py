import pandas as pd 
from sklearn import preprocessing
from sklearn.neighbors import KNeighborsClassifier
from sklearn.model_selection import train_test_split
from sklearn import metrics
import seaborn as sns
import matplotlib.pyplot as plt 

def converssion(data):
    return {
        1:"Basico",
        2:"Moderado",
        3:"Trendic_Topic",
        4:"Empresarial",
        5:"Friki_Tecnologico"
    }[data]

#se lee la base de conocimiento
data=pd.read_csv('data_sample.csv')
#se extraen las columnas que serviran de feature
X = data.iloc[:,0:11]
#la columna Target
y = data.iloc[:,11]
sns.FacetGrid(data, hue="user_type",size=6).map(plt.scatter,"uber","whatsapp").add_legend()
plt.show()
data.plot(kind='scatter',x='uber',y='whatsapp')


#se instancia el algoritmo de kmeans
knn = KNeighborsClassifier(n_neighbors=1)
#se le pasa el frame que procesara y el target en que ubicara
knn.fit(X, y)
#se crea la variable de testeo
#evalua la prediccion
values = knn.predict([[85791444,580846662,0,1673285578,1911423752,863818292,888478324,592819716,554170369,483372459,237548379]])
#imprime la cantidad
print(converssion(int(values)))
