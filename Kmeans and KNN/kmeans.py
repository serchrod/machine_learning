from sklearn.model_selection import train_test_split
import pandas as pd 
from sklearn import preprocessing
from sklearn.cluster import KMeans


data=pd.read_csv('Iris.csv')

features = list(data.columns[1:5])            # SepalLengthCm	SepalWidthCm	PetalLengthCm	PetalWidthCm	
target = data.columns[5]                      # Species


# store feature matrix in "X"
X = data.iloc[:,1:5]                          # slicing: all rows and 1 to 4 cols
# store response vector in "y"
y = data.iloc[:,5]                            # slicing: all rows and 5th col

kmeans = KMeans(n_clusters=2, random_state=0).fit(X)

print(kmeans.predict([[5.9,3.0,5.1,1.8]]))