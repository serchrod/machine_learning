import pandas as pd 
from sklearn import preprocessing
from sklearn import tree
from sklearn import metrics
from sklearn.model_selection import train_test_split

data=pd.read_csv('data_sample.csv')
X = data.iloc[:,0:11]
y = data.iloc[:,11]



clf = tree.DecisionTreeClassifier(criterion='entropy', max_depth=3,min_samples_leaf=5)
clf = clf.fit(X,y)
ypred=clf.predict([[85791444,580846662,0,1673285578,1911423752,863818292,888478324,592819716,554170369,483372459,237548379]])
print(ypred)
