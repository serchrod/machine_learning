import pandas as pd 
from sklearn import preprocessing
from sklearn import tree
from sklearn import metrics
from sklearn.model_selection import train_test_split

data=pd.read_csv('Iris.csv')
X = data.iloc[:,1:5]
y = data.iloc[:,5]

le = preprocessing.LabelEncoder()
le.fit(y)
y=le.transform(y)
data['EncodedSpecies'] = y

X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.4, random_state=4)
clf = tree.DecisionTreeClassifier(criterion='entropy', max_depth=3,min_samples_leaf=5)
clf = clf.fit(X_train,y_train)
ypred=clf.predict([[6, 3, 6, 1]])
print('Predicted Class' , data.loc[data['EncodedSpecies'] == int(ypred), 'Species'].values[0])
