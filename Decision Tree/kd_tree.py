import pandas as pd 
from sklearn import preprocessing
from sklearn.neighbors import KDTree
from sklearn import metrics
import matplotlib.pyplot as plt
import datetime

def converssion(data):
    return {
        1:"Basico",
        2:"Moderado",
        3:"Trendic_Topic",
        4:"Empresarial",
        5:"Friki_Tecnologico"}[data]

data=pd.read_csv('base_model.csv')

# store feature matrix in "X"
X = data.iloc[:,0:7]                          # slicing: all rows and 1 to 4 cols
# store response vector in "y"
y = data.iloc[:,7]                            # slicing: all rows and 5th col
le = preprocessing.LabelEncoder()
le.fit(y)
y=le.transform(y)

# new col
data['EncodedSpecies'] = y

tree = KDTree(X, leaf_size=2)
dist, ind = tree.query([[104857600,734003200,1153433600,1887436800,2097152000,943718400,12582912000]], k=3)
print(dist)
for i in list(ind):
    for k in i:
        print(X.iloc[k])
#knn.fit(X, y)
#value = knn.predict([[6, 3, 6, 1]])
##print(value)
#print( data.loc[data['EncodedSpecies'] == int(value), 'Species'].values[0])

